//
//  ColorRestUITests.swift
//  ColorRestUITests
//
//  Created by Josef Antoni on 23.02.2021.
//

import XCTest

class ColorRestUITests: XCTestCase {

    var colorRest:XCUIApplication?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        self.colorRest = nil
    }
    
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            
        // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }

    func launchApplication(){
        self.colorRest = XCUIApplication()
        self.colorRest?.launch()
    }
    
    /**
     * Basic creating of a single contact.
     * Make sure hardware keyboard is OFF in simulator!
     */
    func testCreateContact() throws {
        self.launchApplication()
        
        let addButton = self.getButtonElementWith("Add")
        addButton.tap()

        let nameTextField = self.getTextFieldElementWith("name")
        nameTextField.tap()
        nameTextField.typeText("Josef")
        
        let emailTextField = self.getTextFieldElementWith("email")
        emailTextField.tap()
        emailTextField.typeText("josef@antoni.cz")

        let descriptionTextField = self.getTextFieldElementWith("description")
        descriptionTextField.tap()
        descriptionTextField.typeText("Just a friendly programmer")

        let createButton = self.getButtonElementWith("Create")
        createButton.tap()
    }
    
    func getTextFieldElementWith(_ string: String) -> XCUIElement {
        let element = self.colorRest!.textFields[string]
        XCTAssertTrue(element.exists);
        return element
    }
    
    func getButtonElementWith(_ string: String) -> XCUIElement {
        let element = self.colorRest!.buttons[string]
        XCTAssertTrue(element.exists);
        return element
    }

}
