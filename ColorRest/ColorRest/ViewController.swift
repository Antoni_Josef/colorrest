//
//  ViewController.swift
//  ColorRest
//
//  Created by Josef Antoni on 23.02.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var contacts: [Contact]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        self.setupTableView()
    }
    
    fileprivate func getData(){
        ServerRequests.shared.getData { (contacts) in
            self.contacts = contacts
            self.tableView.reloadData()
        }
    }

    fileprivate func openDetail(contact: Contact){
        guard let detail = self.storyboard?.instantiateViewController(identifier: "detailViewController") as? DetailViewController else {
            return
        }
        detail.setupWithContact(contact)
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    fileprivate func setupTableView(){
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 30.0;
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func addContactBtn(_ sender: Any) {
        guard let newContactVC = self.storyboard?.instantiateViewController(identifier: "newContactViewController") as? NewContactViewController else {
            return
        }
        self.present(newContactVC, animated: true, completion: nil)
    }
    
    /*
     * Only nonprivate function here, must refresh data that were updated when created new contact
     */
    func contactWasAdded(contact: Contact){
        self.contacts?.append(contact)
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let colorTableCell:ColorTableCell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ColorTableCell
        let color: UIColor = indexPath.row % 2 == 0 ? .randomColor : .white
        let cellData = self.contacts![indexPath.row]
        
        colorTableCell.setupWith(color, name: cellData.name, email: cellData.email)
        return colorTableCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openDetail(contact: self.contacts![indexPath.row])
    }
    
}
