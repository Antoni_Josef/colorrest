//
//  ServerRequests.swift
//  ColorRest
//
//  Created by Josef Antoni on 23.02.2021.
//

import Foundation
import Alamofire
import SwiftyJSON

struct Contact {
    let id: Int
    let email: String
    let name: String
    let description: String
}

class ServerRequests {
    
    fileprivate let url = "https://jsonplaceholder.typicode.com/posts/1/comments"
    static let shared = ServerRequests()
    
    init(){}
    
    func getData(completion: ((_ contacts: [Contact]) -> Void)?){
        AF.request(url).response { response in
            
            var contacts:[Contact] = []

            switch response.result {
            case .success(let data):
                let json = JSON(data ?? Data())
                
                for (_, subJson):(String, JSON) in json {
                    let contact = Contact(id: subJson["id"].intValue,
                                          email: subJson["email"].stringValue,
                                          name: subJson["name"].stringValue,
                                          description: subJson["body"].stringValue
                    )
                    contacts.append(contact)
                }
                
            case .failure(let err):
                print(err)
            }
            
            completion?(contacts)
        }
    }
    
    func createNewContact(contact: Contact, completion: ((_ sucess: Bool) -> Void)?){
        let parameters = [
            "id": contact.id.description,
            "email": contact.email,
            "name": contact.name,
            "body": contact.description
        ]
        
        AF.request(url, method: .post, parameters: parameters).response { response in
            //print(response)

            switch response.result {
            case .success(_):
                completion?(true)
                return
            case .failure(let err):
                print(err)
            }
            completion?(false)
        }

    }
}
