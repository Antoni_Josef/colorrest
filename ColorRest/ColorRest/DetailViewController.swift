//
//  DetailViewController.swift
//  ColorRest
//
//  Created by Josef Antoni on 23.02.2021.
//

import UIKit

class DetailViewController: UIViewController {
        
    fileprivate var contact:Contact?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    func setupWithContact(_ contact: Contact){
        self.contact = contact
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = contact?.name ?? "?"
        emailLabel.text = contact?.email ?? "?"
        descLabel.text = contact?.description ?? "?"
    }
}
