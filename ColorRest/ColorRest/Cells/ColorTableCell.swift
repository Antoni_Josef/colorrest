//
//  ColorTableCell.swift
//  ColorRest
//
//  Created by Josef Antoni on 23.02.2021.
//

import UIKit

class ColorTableCell: UITableViewCell {
    
    @IBOutlet weak var coloredNameLabel: UILabel!
    @IBOutlet weak var coloredEmailLabel: UILabel!

    func setupWith(_ color: UIColor, name: String, email: String){
        self.backgroundColor = color
        self.coloredNameLabel.text = name
        self.coloredEmailLabel.text = email
        
        self.selectionStyle = .none
    }
    
}
