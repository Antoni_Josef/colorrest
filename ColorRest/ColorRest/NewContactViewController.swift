//
//  NewContactViewController.swift
//  ColorRest
//
//  Created by Josef Antoni on 23.02.2021.
//

import UIKit

class NewContactViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var createBtn: UIButton!
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createBtn(_ sender: Any) {
        /*
         *  do not want to mess with contact ids, unless working with real REST API
         */
        guard let emailTextField = emailTextField.text,
              let nameTextField = nameTextField.text,
              let descriptionTextField = descriptionTextField.text,
              emailTextField.count > 0,
              nameTextField.count > 0,
              descriptionTextField.count > 0
        else {
            let alert = UIAlertController(title: "!", message: "To create new contact, you have to fill all displayed information", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)

            return
        }
        let c = Contact(id: 0, email: emailTextField, name: nameTextField, description: descriptionTextField)
        self.createNewContact(contact: c)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupButtons()
    }
    
    fileprivate func createNewContact(contact: Contact){
        ServerRequests.shared.createNewContact(contact: contact) { (success) in
            if(!success){
                let alert = UIAlertController(title: "!", message: "Something went wrong on server side", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)

            } else {
                if let vc = self.presentingViewController?.children.last as? ViewController {
                    vc.contactWasAdded(contact: contact)
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func setupButtons(){
        cancelBtn.layer.cornerRadius = 3
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = UIColor.systemBlue.cgColor
        
        createBtn.layer.cornerRadius = 3
        createBtn.backgroundColor = UIColor.systemBlue
        createBtn.tintColor = UIColor.white
    }
    
}
